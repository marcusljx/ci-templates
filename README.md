# ci-templates

Templates for GitLab CI

## How To Use
```yaml
stages:
  - code-check  # each file requires a particular stage

include:
  - remote: https://gitlab.com/marcusljx/ci-templates/raw/master/go/all-code-check.yml
```